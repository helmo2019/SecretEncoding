package me.helmo2019.secret_encoding;

import me.helmo2019.cli_parser.CLIOption;
import me.helmo2019.cli_parser.CLIParser;
import me.helmo2019.secret_encoding.engines.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static Path file;
    private static EncodingEngine engine;
    private static int mode = 2;
    private static String key, input;

    public static final List<Character> ALPHABET = new ArrayList<>();
    static {
        for(char letter : "abcdefghijklmnopqrstuvwxyz".toCharArray())
            ALPHABET.add(letter);
    }

    public static void main(String[] args) {
        if(args.length < 1)
            printHelp();

        CLIParser parser = new CLIParser(true);
        Map<String, CLIOption> options = parser.getRegisteredOptions();

        options.put("--encode", new CLIOption(0, optArgs -> mode = 0, "--decode", "--both"));
        options.put("--decode", new CLIOption(0, optArgs -> mode = 1, "--encode", "--both"));
        options.put("--both", new CLIOption(0, optArgs -> {}, "--encode", "--decode"));

        options.put("--caesar", new CLIOption(0, optArgs -> engine = new CaesarEncodingEngine(),
                "--vigenere"));
        options.put("--vigenere", new CLIOption(0, optArgs -> engine = new VigenereEncodingEngine(),
                "--caesar"));

        options.put("--key", new CLIOption(0, optArgs -> {
            if(optArgs.length < 1)
                throw new RuntimeException("--key: You need to provide a key");

            key = optArgs[0];
        }));

        options.put("--file", new CLIOption(0, optArgs -> {
            if(optArgs.length < 1) {
                System.err.println("--file: Need to provide a file name!");
                return;
            }

            Path potentialFile = Path.of(optArgs[0]);

            if(!Files.exists(potentialFile)) {
                System.err.println("--file: No such file: "+optArgs[0]);
                return;
            }

            Main.file = potentialFile;
        }));


        parser.parse(args);
        if(!(parser.wasOptionParsed("--caesar") || parser.wasOptionParsed("--vigenere"))) {
            System.err.println("You need to provide an encoding!");
            printHelp();
        }

        if(!parser.wasOptionParsed("--key")) {
            System.err.println("You need to provide a key! (--key)");
            printHelp();
        }

        parser.run(0); // Config

        String input = getInput();

        String messagePart = " '"+input+"' with key '"+key+"' using the "+engine.getName()+" method";
        if(mode == 0) { // Encode
            System.out.println("Encoding"+messagePart);
            System.out.println(engine.encode(input, key));
        } else if(mode == 1) { // Decode
            System.out.println("Decoding"+messagePart);
            System.out.println(engine.decode(input, key));
        } else if(mode == 2) { // Both
            System.out.println("Encoding & Decoding "+messagePart);
            System.out.println(engine.encode(input, key));
            System.out.println(engine.decode(input, key));
        }

    }

    public static char getAlphabetLetter(int id) {
        return ALPHABET.get(id % 26);
    }

    public static int getLetterId(char c) {
        return ALPHABET.indexOf(c);
    }

    public static String stripToOnlyAlphabetical(String s) {
        if(s == null) return "";

        StringBuilder result = new StringBuilder();
        for(char c : s.toCharArray())
            if(ALPHABET.contains(c) || c == ' ') result.append(c);

        return result.toString();
    }

    private static void printHelp() {
        System.out.println(
                """
                Usage:
                
                java -jar secret_encoding.jar [options...]
                
                --key [key]
                    The key to encode/decode with
                
                --file [file]
                    Optional. Reads the text to be
                    encoded/decoded from a file.
                    If omitted, the program will prompt
                    the user for input text.
                
                mode:
                    `--both` (default), `--encode`, or `--decode`
                
                encoding:
                    `--caesar`, or `--vigenere`
                
                """);
        System.exit(0);
    }

    private static String readInputFile() {
        if(file != null) {
            try(BufferedReader reader = Files.newBufferedReader(file)) {
                return stripToOnlyAlphabetical(reader.readLine().toLowerCase());
            } catch (IOException x) {
                throw new RuntimeException(x);
            }
        }

        return null;
    }

    private static String getInput() {
        if(file != null) return readInputFile();

        try (Scanner scan = new Scanner(System.in)) {
            System.out.print("Enter text to be encoded/decoded: ");
            return stripToOnlyAlphabetical(scan.nextLine().toLowerCase());
        }
    }

}

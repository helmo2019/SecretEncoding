package me.helmo2019.secret_encoding.engines;

import me.helmo2019.secret_encoding.Main;

public class VigenereEncodingEngine implements EncodingEngine {
    @Override
    public String decode(String input, String key) {
        return translate(input, key, true);
    }

    @Override
    public String encode(String input, String key) {
        return translate(input, key, false);
    }

    private String translate(String input, String key, boolean decode) {
        input = Main.stripToOnlyAlphabetical(input);
        key = Main.stripToOnlyAlphabetical(key);
        StringBuilder result = new StringBuilder();

        for(int i = 0; i < input.length(); i++) {
            char currentInputChar = input.charAt(i);
            char currentKeyChar = key.charAt(i % key.length());

            result.append(decode ? decodeChar(currentInputChar, currentKeyChar) :
                    encodeChar(currentInputChar, currentKeyChar));
        }

        return result.toString();
    }

    private char encodeChar(char in, char with) {
        if(in == ' ') return in;

        return Main.getAlphabetLetter(
                Main.getLetterId(in)
                        + Main.getLetterId(with)
        );
    }

    private char decodeChar(char encoded, char with) {
        if(encoded == ' ') return encoded;

        return Main.getAlphabetLetter(
                Main.getLetterId(encoded) -
                        Main.getLetterId(with)
                        + 26
        );
    }

    @Override
    public String getName() {
        return "Vigenére Square";
    }
}

package me.helmo2019.secret_encoding.engines;

public interface EncodingEngine {

    String decode(String input, String key);

    String encode(String input, String key);

    String getName();

}

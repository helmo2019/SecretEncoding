package me.helmo2019.secret_encoding.engines;

import me.helmo2019.secret_encoding.Main;

public class CaesarEncodingEngine implements EncodingEngine{
    @Override
    public String decode(String input, String key) {
        return translate(input, key, true);
    }

    @Override
    public String encode(String input, String key) {
        return translate(input, key, false);
    }

    private String translate(String input, String key, boolean subtract) {
        try {
            int shift = Integer.parseInt(key);
            StringBuilder output = new StringBuilder();

            for(char c : input.toCharArray()) {
                if(c == ' ') {
                    output.append(c);
                    continue;
                }

                int charId = Main.ALPHABET.indexOf(c);
                int encodedCharId = charId + (subtract ? -shift : shift);
                output.append(Main.getAlphabetLetter(encodedCharId));
            }

            return output.toString();
        } catch (NumberFormatException x) {
            throw new RuntimeException("Caesar Encoding: Invalid shift: "+key);
        }
    }

    @Override
    public String getName() {
        return "Caesar Shift";
    }
}
